## Geopics

Purpose: help people geotag their pictures.

Main functionalities:
---------------------
#### Sync pictures with a GPX file
- open a GPX file via "GPX File" button
- open a folder with pictures via "Pictures Folder"
- sync they via "Sync" button
- save pictures

#### Drag and drop pictures on a map
- open a folder with pictures via "Pictures Folder"
- drag a picture from pictures list on the map (a pinpoint artefact will be displayed next to picture thumbnail and a marker with the picture will be added to the map)
- save pictures

Additional functionalities:
---------------------------
##### Shift picture and/or gps timestamps
- select a bunch of pictures
- move the sidebars corresponding to hours, mins and seconds
- click "Apply to pictures" or "Apply to GPX"