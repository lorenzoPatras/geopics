const Fs = require('fs');
const Path = require('path');
const Exif = require('piexifjs');
const chai = require('chai');
let expect = chai.expect;
const Picture = require('./../models/picture.js');



describe('Testing the Picture class functionality', function() {
  const currentPath = __dirname;
  const folderPath = Path.join(__dirname, 'test_resources');
  const gpsPictureName = 'gps_info.jpg';
  const noGPSInfoPictureName = 'no_gps_info.jpg';
  const noGPSTagPictureName = 'no_gps_tag.jpg';

  function getMetadata(filePath) {
    console.log(filePath);
    data = Fs.readFileSync(filePath);
    let rawData = data.toString('binary');
    return Exif.load(rawData);
  }

  describe('When you have a picture with gps info in metadata', function () {
    let filePath = Path.join(folderPath, gpsPictureName);
    let metadata = getMetadata(filePath);
    let gpsPicture = new Picture(gpsPictureName, filePath, metadata);

    beforeEach(function() {});

    it('should have gps coordinates different from (0, 0)', function () {
      let gpsPosition = gpsPicture.gpsPosition;
      expect(gpsPosition.latitude).to.not.equal(0);
      expect(gpsPosition.longitude).to.not.equal(0);
    });

    it('should have hasGPSCoords true', function() {
      expect(gpsPicture.hasGPSCoords).to.equal(true);
    });

    afterEach(function(){});
  });

  describe('When you have a picture without gps info in metadata', function () {
    let filePath = Path.join(folderPath, noGPSInfoPictureName);
    let metadata = getMetadata(filePath);
    let gpsPicture = new Picture(noGPSInfoPictureName, filePath, metadata);

    beforeEach(function() {});

    it('should have gps coordinates (0, 0)', function () {
      let gpsPosition = gpsPicture.gpsPosition;
      expect(gpsPosition.latitude).to.equal(0);
      expect(gpsPosition.longitude).to.equal(0);
    });

    it('should have hasGPSCoords flag false', function() {
      expect(gpsPicture.hasGPSCoords).to.equal(false);
    });

    afterEach(function(){});
  });

  describe('When you have a picture without gps info in metadata', function () {
    let filePath = Path.join(folderPath, noGPSTagPictureName);
    let metadata = getMetadata(filePath);
    let gpsPicture = new Picture(noGPSTagPictureName, filePath, metadata);

    beforeEach(function() {});

    it('should have gps coordinates (0, 0)', function () {
      let gpsPosition = gpsPicture.gpsPosition;
      expect(gpsPosition.latitude).to.equal(0);
      expect(gpsPosition.longitude).to.equal(0);
    });

    it('should have hasGPSCoords flag false', function() {
      expect(gpsPicture.hasGPSCoords).to.equal(false);
    });

    afterEach(function(){});
  });

});

// const chai = require('chai');
// const should = chai.should();
// const chaiHttp = require('chai-http');
// chai.use(chaiHttp);
//
// describe('GET /api/v1/users', () => {
//   it('should respond with all users', (done) => {
//
//     chai.request(server)
//       .get('/api/v1/users')
//       .send({
//         user_id: '1',
//         autherization_token = 'f6411750-7529-11e8-ad64-3b3e9fa9ecfc',
//       })
//       .end((err, res) => {
//         // there should be no errors
//         should.not.exist(err);
//         // there should be a 200 status code
//         res.status.should.equal(200);
//         // the response should be JSON
//         res.type.should.equal('application/json');
//         // the JSON response body should have a
//         // key-value pair of {"status": "success"}
//         res.body.status.should.eql('success');
//         // the JSON response body should have a
//         // key-value pair of {"data": [2 user objects]}
//         res.body.data.length.should.eql(2);
//         // the first object in the data array should
//         // have the right keys
//         res.body.data[0].should.include.keys(
//           'id', 'username', 'email', 'created_at'
//         );
//         done();
//       });
//   });
// });
