const Dialog = require('electron').remote.dialog;
const Fs = require('fs');
const Picture = require('./models/picture.js');
const MapManager = require('./managers/map_manager.js');
const GPSManager = require('./managers/gps_manager.js');
const PictureManager = require('./managers/picture_manager.js');
const LeafletController = require('./controllers/leaflet_controller.js');
const DetailsController = require('./controllers/details_controller.js');
const PictureListController = require('./controllers/picture_list_controller.js');


let mapManager = new MapManager();
let gpsManager = new GPSManager(mapManager);
let pictureManager = new PictureManager(gpsManager);
let leafletController = new LeafletController(pictureManager);
let detailsController = new DetailsController();
let pictureListController = new PictureListController(pictureManager, mapManager, detailsController, leafletController);

mapManager.subscribe(leafletController);
// pictureManager.addPictures("/Users/lorenzop/Desktop/test/dumb2");


function importGPX() {
  let gpxFileList = Dialog.showOpenDialog({
    properties: ['openFile'],
    filters: [{
      name: 'Gpx',
      extensions: ['gpx']
    }]
  });

  // cancel case
  if (!gpxFileList) {
    return;
  }

  // gpsFile will be a list but will contain only one element
  let gpxFilePath = gpxFileList[0];
  gpsManager.parseGPXFile(gpxFilePath);
}

function importPictures() {
  let picturesFolderList = Dialog.showOpenDialog({
    properties: ['openDirectory'],
  });

  // 'cancel' case
  if (!picturesFolderList) {
    return;
  }

  picturesFolderPath = picturesFolderList[0];
  pictureManager.addPictures(picturesFolderPath);
}

function syncPicturesWithGPS() {
  pictureManager.syncPicturesWithGPS();
}

function saveAllPictures() {
  pictureManager.saveChangedPictures();
}

function applyOffsetToPictures() {
  let offset = detailsController.getOffsets();
  pictureManager.applyOffsetToSelectedPictures(offset.hour, offset.min, offset.sec);
}

function applyOffsetToGPX() {
  let offset = detailsController.getOffsets();
  gpsManager.shiftGPSTimestamps(offset.hour, offset.min, offset.sec);
}

function changeDate() {
  let dateElem = document.getElementById('pictureDate');
  let date = dateElem.value;
  pictureManager.changeTimestamp(date);
}
