const Path = require('path');
const Fs = require('fs');
const Exif = require('piexifjs');
const DateUtil = require('date-and-time');
const TimeMisc = require('./../util/time_misc.js');

class PictureManager {
  constructor(gpsManager) {
    this.observers = [];
    this.pictures = {};
    this.selectedPictures = new Set();
    this.gpsManager = gpsManager;
    let self = this;
  }

  subscribe(observer) {
    this.observers.push(observer);
  }

  addPictures(folderPath) {
    this.clearPictures();

    Fs.readdir(folderPath, (error, files) => {
      if (error) {
        console.log('error while reading picture file');
        console.log(error);
        return;
      }

      files = files.filter(fileName => ['.jpg', '.jpeg', '.JPG', '.JPEG'].includes(Path.extname(fileName)));

      let promises = [];
      for (let fileName of files) {
        let filePromise = new Promise((resolve, reject) => {
          let fullPath = Path.join(folderPath, fileName);

          Fs.readFile(fullPath, (error, data) => {
            if (error) {
              console.log('error while reading file: ' + fullPath);
              console.log(error);
              return;
            }

            let metadata = Exif.load(data.toString('binary')); // this.logAllMetadataInfo(metadata);
            let picture = new Picture(fileName, fullPath, metadata);
            resolve(picture);
          });
        });
        promises.push(filePromise);
      }

      Promise.all(promises)
        .then(pictures => {
          this.sortPictures(pictures);

          for (let picture of pictures) {
            this.pictures[picture.fileName] = picture;
            this.observers.forEach(observer => observer.addPicture(picture));
          }
        })
        .catch(error => {
          console.log('err ' + error);
        });
    });

  }

  sortPictures(pictures) {
    pictures.sort(function(pic1, pic2) {
      return pic1.dateTaken == pic2.dateTaken ?
        0 :
        (pic1.dateTaken > pic2.dateTaken ? 1 : -1);
    });
  }

  clearPictures() {
    this.pictures = {};
    this.observers.forEach(observer => observer.removeAllPictures());
  }

  addPicture(fileName, folderPath) {
    if (!['.jpg', '.jpeg', '.JPG', '.JPEG'].includes(Path.extname(fileName))) {
      return;
    }

    let fullPath = Path.join(folderPath, fileName);
    Fs.readFile(fullPath, (error, data) => {
      if (error) {
        console.log('error while reading file: ' + fullPath);
        console.log(error);
        return;
      }

      let rawData = data.toString('binary');
      let metadata = Exif.load(rawData);
      let picture = new Picture(fileName, fullPath, metadata);
      // this.logAllMetadataInfo(metadata);
      this.pictures[fileName] = picture;
      this.observers.forEach(observer => observer.addPicture(picture));
    });
  }

  selectSinglePicture(pictureName) {
    this.selectedPictures.clear();
    this.selectedPictures.add(pictureName);
    this.observers.forEach(observer => observer.selectSinglePicture(pictureName));
  }

  getPicture(pictureName) {
    return this.pictures[pictureName];
  }

  syncPicturesWithGPS() {
    let gpsTimestamps = gpsManager.getTimestamps();

    for (let picIdx in this.pictures) {
      let currentPicture = this.pictures[picIdx];

      if (!currentPicture.hasGPSCoords) {
        let closestTimestamp = TimeMisc.findClosestTimestamp(currentPicture.dateTaken.getTime(), gpsTimestamps);

        if (closestTimestamp < 0) {
          continue;
        }

        let closestGPSPoint = gpsManager.getGPSPointAtTimestamp(closestTimestamp);
        currentPicture.updateLocation(closestGPSPoint);
        this.observers.forEach(observer => observer.updatePictureLocation(currentPicture));
      }
    }
  }

  updateSelectedPicturesLocation(newLocation) {
    for (let picName of this.selectedPictures) {
      let selectedPicture = this.pictures[picName];
      selectedPicture.updateLocation(newLocation);
      this.observers.forEach(observer => observer.updatePictureLocation(selectedPicture));
    }
  }

  removePictureSelection() {
    this.observers.forEach(observer => observer.clearSelections());
  }

  saveChangedPictures() {
    for (let picIdx in this.pictures) {
      if (this.pictures[picIdx].isChanged) {
        this.pictures[picIdx].persistChanges();
      }
    }
  }

  applyOffsetToSelectedPictures(hourOffset, minOffset, secOffset) {
    for (let picName of this.selectedPictures) {
      let currentPicture = this.pictures[picName];
      let date = currentPicture.dateTaken;
      let newDate = DateUtil.addHours(DateUtil.addMinutes(DateUtil.addSeconds(date, secOffset), minOffset), hourOffset);
      currentPicture.updateTime(newDate);
      this.observers.forEach(observer => observer.updatePictureTime(currentPicture));
    }
  }

  changeTimestamp(dateStr) {
    let date = new Date(dateStr);
    for (let picName of this.selectedPictures) {
      let currentPicture = this.pictures[picName];
      currentPicture.updateTime(date);
      this.observers.forEach(observer => observer.updatePictureTime(currentPicture));
    }
  }

  togglePicture(pictureName) {
    if (this.selectedPictures.has(pictureName)) {
      this.selectedPictures.delete(pictureName);
      this.observers.forEach(observer => observer.deselectPicture(pictureName));
    } else {
      this.selectedPictures.add(pictureName);
      this.observers.forEach(observer => observer.selectPicture(pictureName));
    }
  }

  // debug purpose
  logAllMetadataInfo(metadata) {
    for (let ifd in metadata) {
      if (ifd == 'thumbnail') {
        continue;
      }
      console.log('-' + ifd);
      for (let tag in metadata[ifd]) {
        console.log('  ' + Exif.Tags[ifd][tag]['name'] + ':' + metadata[ifd][tag]);
      }
    }
  }
}

module.exports = PictureManager;
