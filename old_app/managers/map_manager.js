const DateUtil = require('date-and-time');


class MapManager {
  constructor() {
    this.observers = [];
  }

  subscribe(observer) {
    this.observers.push(observer);
  }

  addGPSPoints(gpsPoints) {
    this.observers.forEach(observer => observer.removeGPSPoints());

    for (let timestamp in gpsPoints) {
      this.observers.forEach(observer => observer.drawGPSPoint(gpsPoints[timestamp]));
    }

    this.moveMapTo(this.getMiddlePoint(gpsPoints));
  }

  moveMapTo(newPosition) {
    this.observers.forEach(observer => observer.moveMapTo(newPosition));
  }

  getMiddlePoint(gpsPoints) {
    let keys = Object.keys(gpsPoints);
    let medianIndex = Math.floor(keys.length / 2);
    return gpsPoints[keys[medianIndex]];
  }
}

module.exports = MapManager;
