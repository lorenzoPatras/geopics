const GpxUtil = require('gps-util');
const GPSPoint = require('./../models/gps_point.js');
const DateUtil = require('date-and-time');


class GPSManager {
  constructor(mapManager) {
    this.gpsPoints = {};
    this.mapManager = mapManager;
  }

  parseGPXFile(filePath) {
    GpxUtil.gpxParseFile(filePath, function(error, result) {
      if (error) {
        console.log('error while parsing GPX file');
        console.log(error);
        return;
      }

      this.gpsPoints = {};
      for (let trackpointIdx in result) {
        let trackpoint = result[trackpointIdx];
        let timestamp = trackpoint.time.getTime();
        this.gpsPoints[timestamp] = new GPSPoint(trackpoint.lat, trackpoint.lng, timestamp);
      }

      this.mapManager.addGPSPoints(this.gpsPoints);
    }.bind(this));
  }


  getTimestamps() {
    return Object.keys(this.gpsPoints);
  }

  getGPSPointAtTimestamp(timestamp) {
    return this.gpsPoints[timestamp];
  }

  shiftGPSTimestamps(hours, minutes, seconds) {
    let newGPSPoints = {};

    for (let timestamp in this.gpsPoints) {
      let gpsPoint = this.gpsPoints[timestamp];
      let gpsDate = new Date(parseInt(gpsPoint.timestamp));
      let newDate = DateUtil.addHours(DateUtil.addMinutes(DateUtil.addSeconds(gpsDate, seconds), minutes), hours);
      let newTimestamp = newDate.getTime();
      gpsPoint.timestamp = newTimestamp;
      newGPSPoints[newTimestamp] = gpsPoint;
    }

    this.gpsPoints = newGPSPoints;
    this.mapManager.addGPSPoints(this.gpsPoints);
  }
}

module.exports = GPSManager;
