const DateUtil = require('date-and-time');
const Path = require('path');


class PictureListController {
  constructor(pictureManager, mapManager, detailsController, leafletController) {
    this.pictureDOMItems = {};
    this.pictureDetailsController = detailsController;
    this.leafletController = leafletController;
    this.pictureManager = pictureManager;
    this.mapManager = mapManager;
    this.lastSelectedItem = null;
    this.lastSelectedIndex = -1;

    this.pictureManager.subscribe(this);
  }

  removeAllPictures() {
    this.pictureDOMItems = {};
    let picturesList = document.getElementById('pictureList');
    while (picturesList.firstChild) {
      picturesList.removeChild(picturesList.firstChild);
    }
  }

  addPicture(picture) {
    let pictureList = document.getElementById('pictureList');

    let picDOM = this.createPictureDom(picture);
    pictureList.appendChild(picDOM);
    this.pictureDOMItems[picture.fileName] = picDOM;

    picDOM.onclick = function(event) { // deep shit o_O
      let nodes = Array.prototype.slice.call(pictureList.children);

      if (event.shiftKey) {
        if (!this.lastSelectedItem) {
          this.lastSelectedItem = picDOM;
          this.lastSelectedIndex = nodes.indexOf(picDOM);
          this.pictureManager.togglePicture(this.lastSelectedItem.textContent);
          return;
        }

        let currentIndex = nodes.indexOf(picDOM);
        if (currentIndex < this.lastSelectedIndex) {
          this.lastSelectedItem = this.lastSelectedItem.previousSibling;
          while (this.lastSelectedItem != picDOM) {
            this.pictureManager.togglePicture(this.lastSelectedItem.textContent);
            this.lastSelectedItem = this.lastSelectedItem.previousSibling;
          }
          this.pictureManager.togglePicture(this.lastSelectedItem.textContent);
        } else {
          this.lastSelectedItem = this.lastSelectedItem.nextSibling;
          while (this.lastSelectedItem != picDOM) {
            this.pictureManager.togglePicture(this.lastSelectedItem.textContent);
            this.lastSelectedItem = this.lastSelectedItem.nextSibling;
          }
          this.pictureManager.togglePicture(this.lastSelectedItem.textContent);
        }
      } else if (event.ctrlKey || event.metaKey) {
        this.lastSelectedItem = picDOM;
        this.lastSelectedIndex = nodes.indexOf(picDOM);
        this.pictureManager.togglePicture(picture.fileName);
      } else {
        this.lastSelectedItem = picDOM;
        this.lastSelectedIndex = nodes.indexOf(picDOM);
        this.pictureManager.selectSinglePicture(picture.fileName);
      }
    }.bind(this);
  }

  updatePictureLocation(picture) {
    let domObject = this.pictureDOMItems[picture.fileName];
    if (!this.hasPinArtefact(domObject)) {
      let pin = document.createElement('div');
      pin.classList.add('pin');
      domObject.appendChild(pin);
    }

    this.pictureDetailsController.fillPictureData(picture);
  }

  updatePictureTime(picture) {
    this.pictureDetailsController.fillPictureData(picture);
  }

  hasPinArtefact(domElement) {
    let children = Array.from(domElement.children);
    for (let idx in children) {
      if (children[idx].classList.contains('pin')) {
        return true;
      }
    }
    return false;
  }

  createPictureDom(picture) {
    let div = document.createElement('div');
    div.classList.add('pictureDOM');

    let img = document.createElement('img');
    img.src = picture.fullPath;
    img.classList.add('thumbnail');
    img.ondragend = this.dragEnd.bind(this);

    let divPictureInfo = document.createElement('div');
    let pictureInfo = document.createElement("p");
    divPictureInfo.classList.add('pictureInfo');
    pictureInfo.appendChild(document.createTextNode(picture.fileName));
    divPictureInfo.appendChild(pictureInfo);

    div.appendChild(img);
    div.appendChild(divPictureInfo);

    if (picture.hasGPSCoords) {
      let pin = document.createElement('div');
      pin.classList.add('pin');
      div.appendChild(pin);
    }

    return div;
  }

  selectSinglePicture(pictureName) {
    this.clearSelections();
    this.pictureDOMItems[pictureName].classList.add('selectedImage');

    let selectedPicture = this.pictureManager.getPicture(pictureName);
    this.pictureDetailsController.fillPictureData(selectedPicture);
    if (selectedPicture.hasGPSCoords) {
      this.mapManager.moveMapTo(selectedPicture.gpsPosition);
    }
  }

  selectPicture(pictureName) {
    this.pictureDOMItems[pictureName].classList.add('selectedImage');
  }

  deselectPicture(pictureName) {
    this.pictureDOMItems[pictureName].classList.remove('selectedImage');
  }

  clearSelections() {
    for (let idx in this.pictureDOMItems) {
      let pictureDOM = this.pictureDOMItems[idx];

      if (pictureDOM.classList.contains('selectedImage')) {
        pictureDOM.classList.remove('selectedImage');
      }
    }
  }

  dragEnd(event) {
    let mouseX = event.clientX;
    let mouseY = event.clientY;

    let newPosition = this.leafletController.transformXYtoGPS(mouseX, mouseY);
    this.pictureManager.updateSelectedPicturesLocation(newPosition);
  }
}

module.exports = PictureListController;
