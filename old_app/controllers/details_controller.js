const DateUtil = require('date-and-time');


class DetailsController {
  constructor() {
    this.hourOffset = 0;
    this.minOffset = 0;
    this.secOffset = 0;

    this.linkSliders();
  }

  linkSliders() {
    let hourSlider = document.getElementById('hourRange');
    let minSlider = document.getElementById('minRange');
    let secSlider = document.getElementById('secRange');

    let hoursDisplay = document.getElementById("hoursOffset");
    let minDisplay = document.getElementById("minOffset");
    let secDisplay = document.getElementById("secOffset");

    hoursDisplay.innerHTML = hourSlider.value;
    minDisplay.innerHTML = minSlider.value;
    secDisplay.innerHTML = secSlider.value;

    hourSlider.oninput = function() {
      hoursDisplay.innerHTML = hourSlider.value;
      this.hourOffset = hourSlider.value;
    }.bind(this);

    minSlider.oninput = function() {
      minDisplay.innerHTML = minSlider.value;
      this.minOffset = minSlider.value;
    }.bind(this);

    secSlider.oninput = function() {
      secDisplay.innerHTML = secSlider.value;
      this.secOffset = secSlider.value;
    }.bind(this);
  }

  getOffsets() {
    return {
      'hour': this.hourOffset,
      'min': this.minOffset,
      'sec': this.secOffset
    };
  }

  fillPictureData(picture) {
    let position = picture.gpsPosition;
    let date = picture.dateTaken;

    let picName = document.getElementById('pictureName');
    picName.innerHTML = picture.fileName;

    let timeElem = document.getElementById('pictureDate');
    timeElem.value = DateUtil.format(date, 'YYYY-MM-DDTHH:mm:ss');

    let latElem = document.getElementById('pictureLatitude');
    latElem.innerHTML = position.latitude;

    let lonElem = document.getElementById('pictureLongitude');
    lonElem.innerHTML = position.longitude;
  }
}

module.exports = DetailsController;
