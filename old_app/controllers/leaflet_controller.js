const Leaflet = require('leaflet');
const DateUtil = require('date-and-time');
const GPSPoint = require('./../models/gps_point.js');


class LeafletController {
  constructor(pictureManager) {
    this.zoomLevel = 7;
    this.map = new Leaflet.Map('mapId');
    this.picturePointsGroup = Leaflet.featureGroup().addTo(this.map);
    this.gpsPointsGroup = Leaflet.featureGroup().addTo(this.map);
    this.pictureMarkers = {};
    this.pictureManager = pictureManager;

    this.pictureManager.subscribe(this);

    this.addMapLayers();
    this.addMapListeners();
  }

  addMapLayers() {
    const defaultLocation = {
      'lat': 47.70,
      'lon': 13.35
    };
    this.map.setView([defaultLocation.lat, defaultLocation.lon], this.zoomLevel);

    let osmSimple = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; OSM Mapnik <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);

    let osmTopo = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
      maxZoom: 17,
      attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
    });

    let mapboxTopo = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.outdoors',
      accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
    });

    let mapboxStreets = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
    });

    let mapLayers = {
      "OSM": osmSimple,
      "OSM topo": osmTopo,
      "Mapbox topo": mapboxTopo,
      "Mapbox streets": mapboxStreets
    };

    Leaflet.control.layers(mapLayers, null).addTo(this.map);
  }

  addMapListeners() {
    this.map.on('zoomend', function(e) {
      this.zoomLevel = e.target._zoom;
    }.bind(this));

    this.map.on('click', function() {
      this.pictureManager.removePictureSelection();
    }.bind(this));
  }

  moveMapTo(newLocation) {
    this.map.setView(this.transformToLatLng(newLocation), this.zoomLevel);
  }

  drawGPSPoint(rawGPSPoint) {
    let gpsPoint = this.transformToLatLng(rawGPSPoint);
    let gpsMarker = Leaflet.circle(gpsPoint, {
      color: 'red',
      fillColor: 'red',
      fillOpacity: 0.5,
      radius: 5
    }).addTo(this.gpsPointsGroup);

    let popup = Leaflet.popup({
        closeButton: false
      })
      .setContent(
        '<p>Timestamp: ' + DateUtil.format(new Date(parseInt(rawGPSPoint.timestamp)), 'HH:mm:ss DD-MM-YYYY') + '<br />' +
        'Latitude: ' + rawGPSPoint.latitude + '<br />' +
        'Longitude: ' + rawGPSPoint.longitude + '<br /></p>');

    gpsMarker.bindPopup(popup);

    gpsMarker.on('mouseover', function(e) {
      this.openPopup();
    });
    gpsMarker.on('mouseout', function(e) {
      this.closePopup();
    });
  }

  removeGPSPoints() {
    this.gpsPointsGroup.clearLayers();
  }

  transformToLatLng(gpsPoint) {
    return new Leaflet.LatLng(gpsPoint.latitude, gpsPoint.longitude)
  }

  addPicture(picture) {
    if (!picture.hasGPSCoords) {
      return;
    }

    let gpsPoint = picture.gpsPosition;
    let marker = new Leaflet.Marker(this.transformToLatLng(gpsPoint), {
      draggable: true,
      icon: Leaflet.divIcon({
        html: "<img src='" + picture.fullPath + "'></img>",
        className: 'pictureIcon'
      })
    }).addTo(this.picturePointsGroup);

    let popup = Leaflet.popup({
        closeButton: false
      })
      .setContent(
        '<p>Timestamp: ' + DateUtil.format(picture.dateTaken, 'HH:mm:ss DD-MM-YYYY') + '<br />' +
        'Latitude: ' + gpsPoint.latitude + '<br />' +
        'Longitude: ' + gpsPoint.longitude + '<br /></p>');

    marker.bindPopup(popup);

    marker.on('mouseover', function(e) {
      this.openPopup();
    });

    marker.on('mouseout', function(e) {
      this.closePopup();
    });

    marker.on('click', function() {
      pictureManager.selectSinglePicture(picture.fileName);
    });

    marker.on('dragstart', function() {
      pictureManager.selectSinglePicture(picture.fileName);
    });

    marker.on('dragend', function() {
      let coord = marker.getLatLng();
      pictureManager.updateSelectedPicturesLocation(new GPSPoint(coord.lat, coord.lng));
    });

    this.pictureMarkers[picture.fileName] = marker;
  }

  updatePictureLocation(picture) {
    if (!(picture.fileName in this.pictureMarkers)) {
      this.addPicture(picture);
    }
  }

  updatePictureTime(picture) {/*ignore*/}

  removeAllPictures() {
    this.pictureMarkers = {};
    this.picturePointsGroup.clearLayers();
  }

  selectSinglePicture(pictureName) {
    this.clearSelections();
    let marker = this.pictureMarkers[pictureName];
    if (marker) { // one can select a picture that does not have a marker on the map
      Leaflet.DomUtil.addClass(marker._icon, 'selectedPicture');
    }
  }

  selectPicture(pictureName) {
    let marker = this.pictureMarkers[pictureName];
    if (marker) { // one can select a picture that does not have a marker on the map
      Leaflet.DomUtil.addClass(marker._icon, 'selectedPicture');
    }
  }

  deselectPicture(pictureName) {
    let marker = this.pictureMarkers[pictureName];
    if (marker && Leaflet.DomUtil.hasClass(marker._icon, 'selectedPicture')) {
      Leaflet.DomUtil.removeClass(marker._icon, 'selectedPicture');
    }
  }

  clearSelections() {
    for (let picName in this.pictureMarkers) {
      this.deselectPicture(picName);
    }
  }

  transformXYtoGPS(x, y) {
    let point = Leaflet.point(x, y);
    let newLatLng = this.map.containerPointToLatLng(point);
    return new GPSPoint(newLatLng.lat, newLatLng.lng);
  }
}

module.exports = LeafletController;
