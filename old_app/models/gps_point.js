class GPSPoint {
  constructor(latitude, longitude, timestamp = 0) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.timestamp = timestamp;
  }
}

module.exports = GPSPoint;
