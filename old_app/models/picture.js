const Fs = require('fs');
const Path = require('path');
const Exif = require('piexifjs');
const DateUtil = require('date-and-time');
const GPSPoint = require('./gps_point.js');


class Picture {
  constructor(fileName, fullPath, metadata) {
    this.fileName = fileName;
    this.fullPath = fullPath;
    let gpsInfo = this.getGPSInfo(metadata);
    this.gpsPosition = gpsInfo.gpsPoint;
    this.hasGPSCoords = gpsInfo.hasGPSCoords;
    this.dateTaken = this.getDate(metadata);
    this.isChanged = false;
  }

  getGPSInfo(metadata) {
    if (!('GPS' in metadata)) {
      return {
        'gpsPoint': new GPSPoint(0, 0),
        'hasGPSCoords': false
      };
    }

    let dmsLatitude = metadata['GPS'][Exif.TagValues.GPSIFD.GPSLatitude];
    let dmsLatitudeRef = metadata['GPS'][Exif.TagValues.GPSIFD.GPSLatitudeRef];
    let dmsLongitude = metadata['GPS'][Exif.TagValues.GPSIFD.GPSLongitude];
    let dmsLongitudeRef = metadata['GPS'][Exif.TagValues.GPSIFD.GPSLongitudeRef];

    if (dmsLatitude == undefined || dmsLatitudeRef == undefined ||
      dmsLongitude == undefined || dmsLongitudeRef == undefined) {
      return {
        'gpsPoint': new GPSPoint(0, 0),
        'hasGPSCoords': false
      };
    }

    let degLatitude = Exif.helper.GPSHelper.dmsRationalToDeg(dmsLatitude, dmsLatitudeRef);
    let degLongitude = Exif.helper.GPSHelper.dmsRationalToDeg(dmsLongitude, dmsLongitudeRef);

    return {
      'gpsPoint': new GPSPoint(degLatitude, degLongitude),
      'hasGPSCoords': true
    };
  }

  getDate(metadata) {
    if (!('Exif' in metadata)) {
      return new Date();
    }

    if (!(Exif.TagValues.ExifIFD.DateTimeOriginal in metadata['Exif'])) {
      return new Date();
    }

    return DateUtil.parse(metadata['Exif'][Exif.TagValues.ExifIFD.DateTimeOriginal], 'YYYY:MM:DD HH:mm:ss')
  }

  updateLocation(newPosition) {
    this.gpsPosition = newPosition;
    this.hasGPSCoords = true;
    this.isChanged = true;
  }

  updateTime(newTime) {
    this.dateTaken = newTime;
    this.isChanged = true;
  }

  persistChanges() {
    if (!this.isChanged) {
      return;
    }

    Fs.readFile(this.fullPath, (error, data) => {
      if (error) {
        console.log('error while reading file: ' + this.fullPath);
        console.log(error);
        return;
      }

      let rawData = data.toString('binary');
      let metadata = Exif.load(rawData);

      let newMetadata = this.composeChangedMetadata(metadata);

      let exifBytes = Exif.dump(newMetadata);
      let newData = Exif.insert(exifBytes, rawData);
      let newJpeg = new Buffer.from(newData, 'binary');
      Fs.writeFile(this.fullPath, newJpeg, (error) => {
        if (error) {
          console.log('error while writing file: ' + this.fullPath);
          console.log(error);
        }
      });
    });
  }

  composeChangedMetadata(metadata) {
    let zeroth = ('0th' in metadata) ? metadata['0th'] : {};
    let first = ('1st' in metadata) ? metadata['1st'] : {};
    let exif = ('Exif' in metadata) ? metadata['Exif'] : {};
    let gps = ('GPS' in metadata) ? metadata['GPS'] : {};

    if (this.hasGPSCoords) {
      gps[Exif.TagValues.GPSIFD.GPSLatitude] = Exif.helper.GPSHelper.degToDmsRational(this.gpsPosition.latitude);
      gps[Exif.TagValues.GPSIFD.GPSLatitudeRef] = this.gpsPosition.latitude < 0 ? 'S' : 'N';
      gps[Exif.TagValues.GPSIFD.GPSLongitude] = Exif.helper.GPSHelper.degToDmsRational(this.gpsPosition.longitude);
      gps[Exif.TagValues.GPSIFD.GPSLongitudeRef] = this.gpsPosition.longitude < 0 ? 'W' : 'E';
    }

    let dateStr = DateUtil.format(this.dateTaken, 'YYYY:MM:DD HH:mm:ss');
    zeroth[Exif.TagValues.ImageIFD.DateTime] = dateStr;
    exif[Exif.TagValues.ExifIFD.DateTimeOriginal] = dateStr;
    exif[Exif.TagValues.ExifIFD.DateTimeDigitized] = dateStr;

    return {
      '0th': zeroth,
      '1st': first,
      'Exif': exif,
      'GPS': gps
    };
  }
}

module.exports = Picture;
