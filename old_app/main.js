const {
  app,
  BrowserWindow
} = require('electron')

let win // without it the window will be garbage collected

function createWindow() {
  win = new BrowserWindow({
    width: 1200,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
  }
  })
  win.loadFile('index.html')
  // win.webContents.openDevTools() // for debugging

  win.on('closed', () => {
    win = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  // if you want the mac stupid functionality of keeping the app alive after close
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // used to recover in case you are on mac and have enabled the keep alive application
  if (win === null) {
    createWindow()
  }
})
