module.exports.findClosestTimestamp = function(date, datesList) {
  let intDates = toIntList(datesList);
  let closest = intDates.reduce(function(prev, curr) {
    return (Math.abs(curr - date) < Math.abs(prev - date) ? curr : prev);
  });

  let oneMinThreshold = 60000;
  if (Math.abs(closest - date) > oneMinThreshold) { // if there is no close timestamp discard
    return -1;
  }

  return closest;
}

function toIntList(strDates) {
  let intDates = strDates.map(function(strDate) {
    return parseInt(strDate);
  });

  return intDates;
}
